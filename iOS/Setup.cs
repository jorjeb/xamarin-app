﻿using System;

using MvvmCross;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views;
using MvvmCross.Forms.Presenter.iOS;
using MvvmCross.Platform;

namespace Demo.iOS
{
	public class Setup : MvxIosSetup
	{
		public Setup(MvxApplicationDelegate applicationDelegate, UIWindow window) : base(applicationDelegate, window)
		{
		}

		protected override IMvxApplication CreateApp()
		{
			return new App();
		}

		protected override IMvxTrace CreateDebugTrace()
		{
			return new DebugTrace();
		}

		protected override IMvxIosViewPresenter CreatePresenter()
		{
			Forms.Init();

			var formsApp = new FormsApp();

			return new MvxFormsIosPagePresenter(Window, formsApp);
		}

		protected override void InitializeIoC()
		{
			base.InitializeIoC();

			Mvx.RegisterSingleton(() => UserDialogs.Instance);
		}
	}
}
