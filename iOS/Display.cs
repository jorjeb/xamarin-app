﻿using System;

using Xamarin.Forms;

using UIKit;

using Demo.iOS;
using Demo.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(Display))]

namespace Demo.iOS
{
	public class Display : IDisplay
	{
		public Display()
		{
		}

		public void SetStatusBarColor(string hex)
		{
		}

		public double GetScreenWidth()
		{
			return UIScreen.MainScreen.Bounds.Width;
		}
	}
}
