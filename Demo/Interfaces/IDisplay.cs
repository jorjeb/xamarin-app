﻿using System;

namespace Demo.Interfaces
{
	public interface IDisplay
	{
		void SetStatusBarColor(string hex);
		double GetScreenWidth();
	}
}
