﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

using Demo.Views.Business;
using Demo.Pages;

namespace Demo.Views.Account
{
	public partial class Login : ContentView
	{
		public Login()
		{
			InitializeComponent();
		}

		void selectBusinesses(object sender, EventArgs args)
		{
			var selectBusinessesView = new Select();
			var businessPage = new Pages.Business();

			businessPage.Content = selectBusinessesView;

			Navigation.PushAsync(businessPage);
		}
	}
}
