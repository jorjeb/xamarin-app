﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

using Demo.Pages;

namespace Demo.Views.Business
{
	public partial class Select : ContentView
	{
		public Select()
		{
			InitializeComponent();

			this.BindingContext = new object[] {
				new {
					Title = "Kayo",
					SubTitle = "Kayo"
				},
				new {
					Title = "Insulive",
					SubTitle = "Insulive"
				},
				new {
					Title = "Avadel",
					SubTitle = "Avadel"
				},
				new {
					Title = "Micee",
					SubTitle = "Micee"
				},
				new {
					Title = "Cervor",
					SubTitle = "Cervor"
				},
				new {
					Title = "Antido",
					SubTitle = "Polyent"
				},
				new {
					Title = "Rhyosis",
					SubTitle = "Rhyosis"
				},
				new {
					Title = "Neombu",
					SubTitle = "Neombu"
				},
				new {
					Title = "Demivu",
					SubTitle = "Demivu"
				},
				new {
					Title = "Albore",
					SubTitle = "Albore"
				},
				new {
					Title = "Platz",
					SubTitle = "Platz"
				}
			};
		}

		void createBusiness(object sender, EventArgs args)
		{
			var createBusinessView = new Create();
			var businessPage = new Pages.Business();

			businessPage.Content = createBusinessView;

			Navigation.PushAsync(businessPage);
		}
	}
}
