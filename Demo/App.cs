﻿using System;

using MvvmCross.Platform.IoC;
using MvvmCross.Core.ViewModels;

namespace Demo
{
	public class App : MvxApplication
	{
		public override void Initialize()
		{
			CreatableTypes()
				.EndingWith("Service")
				.AsInterfaces()
				.RegisterAsLazySingleton();

			RegisterAppStart<ViewModels.FirstViewModel>();
		}
	}
}
