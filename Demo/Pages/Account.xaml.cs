﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Demo.Interfaces;

namespace Demo.Pages
{
	public partial class Account : ContentPage
	{
		public Account()
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);

			if (Device.OS == TargetPlatform.Android)
			{
				DependencyService.Get<IDisplay>().SetStatusBarColor("#313a51");
			}
		}
	}
}
