﻿using System;

using Xamarin.Forms;

using SkiaSharp;

using Demo.Components;
using Demo.Interfaces;

namespace Demo.Pages
{
	public partial class DemoPage : ContentPage
	{
		public DemoPage()
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);

			var background = new Background(new[] {
				new SKColor(74, 65, 160),
				new SKColor(44, 42, 44)
			});
			background.HorizontalOptions = LayoutOptions.FillAndExpand;
			background.VerticalOptions = LayoutOptions.FillAndExpand;

			var dial = new Dial(new[] {
				new SKColor(45, 135, 20)
			}, DependencyService.Get<IDisplay>().GetScreenWidth());
			dial.HorizontalOptions = LayoutOptions.FillAndExpand;
			dial.VerticalOptions = LayoutOptions.FillAndExpand;

			var cookButton = new Dial.Button()
			{
				Source = ImageSource.FromFile("cook.png")
			};
			cookButton.Clicked += delegate(object sender, EventArgs args) 
			{
				Navigation.PushAsync(new DemoPage2());
			};
			dial.AddButton(cookButton);

			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("handwash.png")
			});

			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("spray_bottle.png")
			});

			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("scan_qr_code.png")
			});

			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("purchase.png")
			});

			var layout = new RelativeLayout();

			layout.Children.Add(background, Constraint.RelativeToParent((parent) =>
			{
				return parent.X;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Y;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Width;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Height;
			}));

			layout.Children.Add(dial, Constraint.RelativeToParent((parent) =>
			{
				return parent.X;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Y;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Width;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Height;
			}));

			Content = layout;
		}
	}
}
