﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Demo.Pages
{
	public partial class Business : ContentPage
	{
		public Business()
		{
			InitializeComponent();

			NavigationPage.SetHasBackButton(this, false);

			//var navigationPage = ((NavigationPage)App.Current.MainPage);
			//navigationPage.BarBackgroundColor = Color.FromHex("#313a51");
			//navigationPage.BarTextColor = Color.White;

			Title = "Select Business";
		}
	}
}
