﻿using System;

using Xamarin.Forms;

using SkiaSharp;

using Demo.Components;
using Demo.Interfaces;

namespace Demo.Pages
{
	public partial class DemoPage2 : ContentPage
	{
		int cursor = 0;

		public DemoPage2()
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);

			var tasks = new string[]{
				"Task 1",
				"Task 2",
				"Task 3",
				"Task 4",
				"Task 5",
				"Task 6",
				"Task 7",
				"Task 8",
				"Task 9"
			};

			var tasksList = new ListView();

			if (Device.OS == TargetPlatform.Android)
			{
				var cell = new DataTemplate(typeof(TextCell));
				cell.SetBinding(TextCell.TextProperty, ".");
				cell.SetValue(TextCell.TextColorProperty, Color.White);
				tasksList.ItemTemplate = cell;
			}

			tasksList.ItemsSource = tasks;
			tasksList.SelectedItem = tasks[0];

			var background = new Background(new[] {
				new SKColor(74, 65, 160)
			});
			background.HorizontalOptions = LayoutOptions.FillAndExpand;
			background.VerticalOptions = LayoutOptions.FillAndExpand;

			var dial = new Dial(new[] {
				new SKColor(88, 120, 251)
			}, DependencyService.Get<IDisplay>().GetScreenWidth(), true);
			dial.HorizontalOptions = LayoutOptions.FillAndExpand;
			dial.VerticalOptions = LayoutOptions.FillAndExpand;

			var selectButton = new Dial.Button()
			{
				Source = ImageSource.FromFile("select.png")
			};
			selectButton.Clicked += delegate (object sender, EventArgs args)
			{
				Navigation.PushAsync(new DemoPage3(tasks[cursor]));
			};	
			dial.AddButton(selectButton);

			var firstButton = new Dial.Button()
			{
				Source = ImageSource.FromFile("first.png")
			};
			firstButton.Clicked += delegate (object sender, EventArgs args)
			{
				cursor = 0;
				tasksList.SelectedItem = tasks[cursor];
				tasksList.ScrollTo(tasks[cursor], ScrollToPosition.Center, true);
			};
			dial.AddButton(firstButton);

			var upButton = new Dial.Button()
			{
				Source = ImageSource.FromFile("up.png")
			};
			upButton.Clicked += delegate (object sender, EventArgs args)
			{
				cursor--;

				if (cursor < 0)
				{
					cursor = 0;
				}

				tasksList.SelectedItem = tasks[cursor];
				tasksList.ScrollTo(tasks[cursor], ScrollToPosition.Center, true);
			};
			dial.AddButton(upButton);

			var downButton = new Dial.Button()
			{
				Source = ImageSource.FromFile("down.png")
			};
			downButton.Clicked += delegate (object sender, EventArgs args)
			{
				cursor++;

				if (cursor > (tasks.Length - 1))
				{
					cursor = tasks.Length - 1;
				}
				tasksList.SelectedItem = tasks[cursor];
				tasksList.ScrollTo(tasks[cursor], ScrollToPosition.Center, true);
			};
			dial.AddButton(downButton);

			var lastButton = new Dial.Button()
			{
				Source = ImageSource.FromFile("last.png")
			};
			lastButton.Clicked += delegate (object sender, EventArgs args)
			{
				cursor = tasks.Length - 1;
				tasksList.SelectedItem = tasks[cursor];
				tasksList.ScrollTo(tasks[cursor], ScrollToPosition.Center, true);
			};
			dial.AddButton(lastButton);

			var returnButton = new Dial.Button()
			{
				Source = ImageSource.FromFile("return_arrow.png")
			};
			returnButton.Clicked += delegate (object sender, EventArgs args)
			{
				Navigation.PopAsync();
			};
			dial.AddButton(returnButton);

			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("read.png")
			});

			dial.contentArea.Content = tasksList;

			var layout = new RelativeLayout();

			layout.Children.Add(background, Constraint.RelativeToParent((parent) =>
			{
				return parent.X;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Y;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Width;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Height;
			}));

			layout.Children.Add(dial, Constraint.RelativeToParent((parent) =>
			{
				return parent.X;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Y;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Width;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Height;
			}));

			Content = layout;
		}
	}
}
