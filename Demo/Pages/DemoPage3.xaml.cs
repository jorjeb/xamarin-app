﻿using System;

using Xamarin.Forms;

using SkiaSharp;

using Demo.Components;
using Demo.Interfaces;

namespace Demo.Pages
{
	public partial class DemoPage3 : ContentPage
	{
		public DemoPage3(string task)
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);

			var background = new Background(new[] {
				new SKColor(74, 65, 160)
			});
			background.HorizontalOptions = LayoutOptions.FillAndExpand;
			background.VerticalOptions = LayoutOptions.FillAndExpand;

			var dial = new Dial(new[] {
				new SKColor(251, 13, 33),
				new SKColor(13, 36, 250)
			}, DependencyService.Get<IDisplay>().GetScreenWidth(), true, true);
			dial.HorizontalOptions = LayoutOptions.FillAndExpand;
			dial.VerticalOptions = LayoutOptions.FillAndExpand;

			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("cold_temp_warning.png")
			});
			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("temp_6_0.png")
			});
			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("temp_5_5.png")
			});
			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("temp_5_0.png")
			});
			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("temp_4_5.png")
			});
			dial.AddButton(new Dial.Button()
			{
				Source = ImageSource.FromFile("temp_4_0.png")
			});
			var returnButton = new Dial.Button()
			{
				Source = ImageSource.FromFile("return_arrow.png")
			};
			returnButton.Clicked += delegate (object sender, EventArgs args)
			{
				Navigation.PopAsync();
			};
			dial.AddButton(returnButton);

			var selectedTask = new Label()
			{
				TextColor = Color.White
			};
			selectedTask.Text = task;

			dial.contentArea.Padding = new Thickness(15, 13);
			dial.contentArea.Content = selectedTask;

			var layout = new RelativeLayout();

			layout.Children.Add(background, Constraint.RelativeToParent((parent) =>
			{
				return parent.X;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Y;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Width;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Height;
			}));

			layout.Children.Add(dial, Constraint.RelativeToParent((parent) =>
			{
				return parent.X;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Y;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Width;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Height;
			}));

			Content = layout;
		}
	}
}
