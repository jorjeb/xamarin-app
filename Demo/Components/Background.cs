﻿using System;

using Xamarin.Forms;

using SkiaSharp;
using SkiaSharp.Views.Forms;

namespace Demo.Components
{
	public class Background : ContentView
	{
		SKColor[] colors;

		public Background(SKColor[] colors)
		{
			this.colors = colors;

			var canvasView = new SKCanvasView();
			canvasView.PaintSurface += OnCanvasViewPaintSurface;
			Content = canvasView;
		}

		void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
		{
			var info = args.Info;
			var surface = args.Surface;
			var canvas = surface.Canvas;

			using (var paint = new SKPaint())
			{
				paint.IsAntialias = true;

				if (colors.Length > 1)
				{
					using (var shader = SKShader.CreateLinearGradient(
						new SKPoint(0, 0),
						new SKPoint(0, info.Height),
						this.colors,
						null,
						SKShaderTileMode.Clamp))
					{
						paint.Shader = shader;
						canvas.DrawPaint(paint);
					}
				}
				else if (colors.Length == 1)
				{
					paint.Color = this.colors[0];
					canvas.DrawPaint(paint);
				}
			}
		}
	}
}

