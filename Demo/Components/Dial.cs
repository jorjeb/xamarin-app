﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using SkiaSharp;
using SkiaSharp.Views.Forms;

using XFShapeView;

namespace Demo.Components
{
	public class Dial : ContentView
	{
		SKColor[] colors;
		double screenWidth;
		bool showContentArea;
		bool enableRotation;
		public ContentView contentArea;
		List<Button> buttons;
		AbsoluteLayout contentLayout;
		AbsoluteLayout buttonsLayout;
		float radius;
		float strokeWidth;
		SKRect contentRect;
		SKPoint center;
		SKPoint screenEdge;
		double startingAngle;
		double totalAngle;
		ShapeView circle;

		public Dial(SKColor[] colors, double screenWidth, bool showContentArea = false, bool enableRotation = false, List<Button> buttons = null)
		{
			this.colors = colors;
			this.screenWidth = screenWidth;
			this.showContentArea = showContentArea;
			this.enableRotation = enableRotation;

			this.buttons = new List<Button>();

			var layout = new RelativeLayout();

			var canvasView = new SKCanvasView();
			canvasView.PaintSurface += OnCanvasViewPaintSurface;
			layout.Children.Add(canvasView, Constraint.RelativeToParent((parent) =>
			{
				return parent.X;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Y;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Width;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Height;
			}));

			contentLayout = new AbsoluteLayout();

			if (buttons != null)
			{
				foreach (var button in buttons)
				{
					AddButton(button);
				}
			}

			layout.Children.Add(contentLayout, Constraint.RelativeToParent((parent) =>
			{
				return parent.X;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Y;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Width;
			}), Constraint.RelativeToParent((parent) =>
			{
				return parent.Height;
			}));

			if (showContentArea)
			{
				contentArea = new ContentView();
			}

			Content = layout;
		}

		void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
		{
			var info = args.Info;
			var surface = args.Surface;
			var canvas = surface.Canvas;

			canvas.Clear();

			using (var paint = new SKPaint())
			{
				paint.IsAntialias = true;
				paint.Style = SKPaintStyle.Stroke;

				paint.StrokeWidth = info.Width / 4;
				radius = info.Width / 3 * 2;

				strokeWidth = paint.StrokeWidth;

				center = new SKPoint()
				{
					X = info.Width + (info.Width / 10),
					Y = info.Height - (info.Width / 6)
				};

				screenEdge = new SKPoint()
				{
					X = info.Width,
					Y = info.Height
				};

				paint.Color = SKColors.White;
				canvas.DrawCircle(center.X, center.Y, radius, paint);

				paint.StrokeWidth = info.Width / 4 - 10;
				var innerRadius = info.Width / 3 * 2;

				if (colors.Length > 1)
				{
					using (var shader = SKShader.CreateLinearGradient(
						new SKPoint(info.Width - (info.Width / 10), info.Height - (info.Width / 6) - innerRadius - paint.StrokeWidth),
						new SKPoint(info.Width - ((info.Width + (info.Width / 10)) - innerRadius - paint.StrokeWidth), info.Height),
						this.colors,
						null,
						SKShaderTileMode.Clamp))
					{
						paint.Shader = shader;
						canvas.DrawCircle(info.Width + (info.Width / 10), info.Height - (info.Width / 6), innerRadius, paint);
					}
				}
				else if (colors.Length == 1)
				{
					paint.Color = this.colors[0];
					canvas.DrawCircle(info.Width + (info.Width / 10), info.Height - (info.Width / 6), innerRadius, paint);
				}

				if (showContentArea)
				{
					paint.Shader = null;

					contentRect = SKRect.Create(new SKPoint(100, 80), new SKSize(info.Width - 200, (info.Height - radius - strokeWidth) - 220));

					paint.Style = SKPaintStyle.Fill;
					paint.Color = SKColors.Black;
					canvas.DrawRoundRect(contentRect, 10f, 10f, paint);

					paint.Style = SKPaintStyle.Stroke;
					paint.StrokeWidth = 5;
					paint.Color = SKColors.White;
					canvas.DrawRoundRect(contentRect, 10f, 10f, paint);
				}
			}

			LayoutComponents();
		}

		public void AddButton(Button button)
		{
			button.BackgroundColor = Color.Transparent;
			buttons.Add(button);
		}

		void LayoutComponents()
		{
			var center = NormalizePoint(this.center);
			var screenEdge = NormalizePoint(this.screenEdge);
			var radius = Normalize(this.radius);
			var strokeWidth = Normalize(this.strokeWidth);

			var point1 = screenEdge.Y - (Math.Sqrt(Math.Pow(radius, 2) - Math.Pow(center.X - screenEdge.X, 2)) + (screenEdge.Y - center.Y));
			var point2 = screenEdge.X - Math.Sqrt(Math.Pow(radius, 2) - Math.Pow(screenEdge.Y - center.Y, 2));

			var angle1 = Math.Atan2(center.Y - point1, center.X - screenEdge.X) + Math.PI;
			var angle2 = Math.Atan2(center.Y - screenEdge.Y, center.X - point2) + Math.PI;

			var angle = angle2 - angle1;

			var anglePadding = (angle / (Math.Min(buttons.Count, 8) - 1)) / 2;
			angle1 += anglePadding;
			angle -= anglePadding * 2;

			var innerRadius = radius - 46;

			circle = new ShapeView()
			{
				ShapeType = ShapeType.Circle,
				HeightRequest = (innerRadius + strokeWidth) * 2,
				WidthRequest = (innerRadius + strokeWidth) * 2,
				Color = Color.Transparent
			};

			var panGesture = new PanGestureRecognizer();
			panGesture.PanUpdated += OnPanUpdated;

			if (enableRotation)
			{
				circle.GestureRecognizers.Add(panGesture);
			}

			AbsoluteLayout.SetLayoutBounds(circle, new Rectangle(center.X - innerRadius - strokeWidth, center.Y - innerRadius - strokeWidth, (innerRadius + strokeWidth) * 2, (innerRadius + strokeWidth) * 2));
			contentLayout.Children.Add(circle);

			innerRadius = radius - 44;

			var innerCircle = new ShapeView()
			{
				ShapeType = ShapeType.Circle,
				HeightRequest = innerRadius * 2,
				WidthRequest = innerRadius * 2,
				Color = Color.Transparent
			};

			AbsoluteLayout.SetLayoutBounds(innerCircle, new Rectangle(center.X - innerRadius, center.Y - innerRadius, innerRadius * 2, innerRadius * 2));
			contentLayout.Children.Add(innerCircle);

			buttonsLayout = new AbsoluteLayout();

			circle.Content = buttonsLayout;
			center = new SKPoint()
			{
				X = (float)buttonsLayout.Width / 2,
				Y = (float)buttonsLayout.Height / 2
			};

			var prevX = (center.X) + (radius * Math.Cos(angle1 - (angle / (Math.Min(buttons.Count, 8) - 1))));
			var prevY = (center.Y) + (radius * Math.Sin(angle1 - (angle / (Math.Min(buttons.Count, 8) - 1))));

			var tapGesture = new TapGestureRecognizer();
			tapGesture.Tapped += OnTapped;

			foreach (var button in buttons)
			{
				if (enableRotation)
				{
					button.GestureRecognizers.Add(panGesture);
				}
				else
				{
					button.GestureRecognizers.Add(tapGesture);
				}
			}

			var hiddenButtonCount = 0d;

			if (buttons.Count > 8)
			{
				hiddenButtonCount = Math.Ceiling((buttons.Count - 8) / 2d);
			}

			if (hiddenButtonCount > 0)
			{
				angle1 = angle1 - ((angle / 7) * hiddenButtonCount);
				prevX = (center.X) + (radius * Math.Cos(angle1 - (angle / (Math.Min(buttons.Count, 8) - 1))));
				prevY = (center.Y) + (radius * Math.Sin(angle1 - (angle / (Math.Min(buttons.Count, 8) - 1))));

				for (var i = 0; i < hiddenButtonCount; i++)
				{
					var button = buttons[i];
					var anglePart = angle1 + ((angle / 7) * i);

					var x = (center.X) + (radius * Math.Cos(anglePart));
					var y = (center.Y) + (radius * Math.Sin(anglePart));

					var buttonSize = (int)Math.Sqrt(Math.Pow(x - prevX, 2) + Math.Pow(y - prevY, 2)) - 8;

					prevX = x;
					prevY = y;

					button.WidthRequest = buttonSize;
					button.HeightRequest = buttonSize;

					AbsoluteLayout.SetLayoutBounds(button, new Rectangle(x - (buttonSize / 2), y - (buttonSize / 2), buttonSize, buttonSize));

					buttonsLayout.Children.Add(button);
				}
			}

			for (var i = (hiddenButtonCount > 0 ? (int)hiddenButtonCount : 0); i < buttons.Count; i++)
			{
				var button = buttons[i];
				var anglePart = angle1 + ((angle / (Math.Min(buttons.Count, 8) - 1)) * i);

				var x = (center.X) + (radius * Math.Cos(anglePart));
				var y = (center.Y) + (radius * Math.Sin(anglePart));

				var buttonSize = (int)Math.Sqrt(Math.Pow(x - prevX, 2) + Math.Pow(y - prevY, 2)) - 8;

				prevX = x;
				prevY = y;

				button.WidthRequest = buttonSize;
				button.HeightRequest = buttonSize;

				AbsoluteLayout.SetLayoutBounds(button, new Rectangle(x - (buttonSize / 2), y - (buttonSize / 2), buttonSize, buttonSize));

				buttonsLayout.Children.Add(button);
			}

			if (showContentArea)
			{
				AbsoluteLayout.SetLayoutBounds(contentArea, new Rectangle(Normalize(contentRect.Left) + 1, Normalize(contentRect.Top) + 1, Normalize(contentRect.Width) - 2, Normalize(contentRect.Height) - 2));
				contentLayout.Children.Add(contentArea);
			}
		}

		private SKPoint NormalizePoint(SKPoint point)
		{
			return new SKPoint()
			{
				X = (float)Normalize(point.X),
				Y = (float)Normalize(point.Y)
			};
		}

		private double Normalize(double pixels)
		{
			var screenHeight = (screenWidth * this.screenEdge.Y) / this.screenEdge.X;

			return (pixels / (Math.Sqrt(Math.Pow(screenEdge.X, 2) + Math.Pow(screenEdge.Y, 2)) / Math.Sqrt(Math.Pow(screenWidth, 2) + Math.Pow(screenHeight, 2))));
		}

		void OnTapped(object sender, EventArgs args)
		{
			if (sender is Button)
			{
				((Button)sender).triggerClick(sender, args);
			}
		}

		void OnPanUpdated(object sender, PanUpdatedEventArgs args)
		{
			switch (args.StatusType)
			{
				case GestureStatus.Started:
					startingAngle = getAngle(args.TotalX, args.TotalY);
					totalAngle = 0;
					break;
				case GestureStatus.Running:
					double currentAngle = getAngle(args.TotalX, args.TotalY);

					rotateDial((float)(startingAngle - currentAngle));

					totalAngle += startingAngle - currentAngle;
					startingAngle = currentAngle;
					break;
				case GestureStatus.Completed:
					if (Math.Abs(totalAngle) < 1.5) // It's a tap!
					{
						if (sender is Button)
						{
							((Button)sender).triggerClick(sender, args);
						}
					}
					break;
			}
		}

		private double getAngle(double xTouch, double yTouch)
		{
			double x = xTouch - (circle.Width / 2);
			double y = circle.Width - yTouch - (circle.Height / 2);

			var degrees = Math.Asin(y / Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2))) * (180 / Math.PI);

			switch (getQuadrant(x, y))
			{
				case 1:
					return degrees;
				case 2:
					return 180 - degrees;
				case 3:
					return 180 + (-1 * degrees);
				case 4:
					return 360 + degrees;
				default:
					return 0;
			}
		}

		private int getQuadrant(double x, double y)
		{
			var center = NormalizePoint(this.center);

			if (x >= center.X)
			{
				return y >= center.Y ? 1 : 4;
			}
			else
			{
				return y >= center.Y ? 2 : 3;
			}
		}

		private void rotateDial(float degrees)
		{
			buttonsLayout.Rotation += degrees;

			foreach (var button in buttons)
			{
				button.Rotation -= degrees;
			}
		}

		public class Button : Image
		{
			public event EventHandler Clicked;

			public void triggerClick(object sender, EventArgs args)
			{
				if (Clicked != null)
				{
					Clicked(sender, args);
				}
			}
		}
	}
}

