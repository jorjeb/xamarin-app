﻿using Xamarin.Forms;

using MvvmCross.Forms.Presenter.Core;

using Demo.Views.Account;
using Demo.Pages;

namespace Demo
{
	public partial class FormsApp : MvxFormsApp
	{
		public FormsApp()
		{
			InitializeComponent();

			var loginView = new Login();
			var accountPage = new Account();

			accountPage.Content = loginView;

			MainPage = new NavigationPage(accountPage);
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
