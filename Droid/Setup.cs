﻿using System;

using Android.Content;

using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using MvvmCross.Droid.Views;
using MvvmCross.Forms.Presenter.Droid;
using MvvmCross.Platform;

namespace Demo.Droid
{
	public class Setup : MvxAndroidSetup
	{
		public Setup(Context applicationContext) : base(applicationContext)
		{
		}

		protected override IMvxApplication CreateApp()
		{
			return new App();
		}

		protected override IMvxTrace CreateDebugTrace()
		{
			return new DebugTrace();
		}

		protected override IMvxAndroidViewPresenter CreateViewPresenter()
		{
			var presenter = new MvxFormsDroidPagePresenter();

			Mvx.RegisterSingleton<IMvxViewPresenter>(presenter);

			return presenter;
		}

		protected override void InitializeIoC()
		{
			base.InitializeIoC();

			Mvx.RegisterSingleton(() => UserDialogs.Instance);
		}
	}
}
