﻿using System;

using Xamarin.Forms;

using Demo.Droid;
using Demo.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(Display))]

namespace Demo.Droid
{
	public class Display : IDisplay
	{
		public Display()
		{
		}

		public void SetStatusBarColor(string hex)
		{
			var context = Forms.Context as MainActivity;

			if (context != null) // resolves to null when inside Xamarin preview
			{
				context.Window.SetStatusBarColor(Android.Graphics.Color.ParseColor(hex));
			}
		}

		public double GetScreenWidth()
		{
			var context = Forms.Context as MainActivity;

			var pixels = context.Resources.DisplayMetrics.WidthPixels; // real pixels
			var scale = context.Resources.DisplayMetrics.Density;

			return (pixels - 0.5f) / scale;
		}
	}
}
