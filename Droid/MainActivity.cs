﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using CarouselView.FormsPlugin.Android;

using MvvmCross.Droid.Views;
using MvvmCross.Forms.Presenter.Droid;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;

namespace Demo.Droid
{
	[Activity(Label = "Demo.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			global::Xamarin.Forms.Forms.Init(this, bundle);
			CarouselViewRenderer.Init();

			Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

			var formsApp = new FormsApp();
			LoadApplication(formsApp);

			var presenter = (MvxFormsDroidPagePresenter)Mvx.Resolve<IMvxAndroidViewPresenter>();
			presenter.MvxFormsApp = formsApp;

			Mvx.Resolve<IMvxAppStart>().Start();
		}
	}
}
